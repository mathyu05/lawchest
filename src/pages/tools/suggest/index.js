import React from "react";
import { graphql, useStaticQuery } from "gatsby";
import ToolsLayout from "../../../components/toolsLayout";
import SEO from "../../../components/seo";
import Apollo from "../../../components/apollo";
import ToolSuggestionForm from "../../../components/suggest/toolSuggestionForm.js";
import SuggestionIllustration from "../../../images/suggestion-illustration.svg";
import { suggestToolMutation } from "../../../utils/staticQueryFragments";

const ToolsSuggestPage = (props) =>
{
  const { site } = useStaticQuery(
    graphql`
      query
      {
        site
        {
          siteMetadata
          {
            tools
            {
              id
              name
              description
              link
            }
          }
        }
      }
    `
  );

  const tool = site.siteMetadata.tools.filter(t => t.id === 999)[0];

  return (
    <ToolsLayout
      show = { true }
      name = { tool.name }
      description = { tool.description }
      icon = { SuggestionIllustration }
      goBackDestination = "/tools"
      goBackText = "← Back to Tools Menu"
    >
      <SEO
        keywords = { [`LawChest`, `tool`, `suggest`, `suggestion`] }
        title = "Suggest a Tool"
      />

      <section
        id = "utilitySection"
      >
        <Apollo
          propsToInsert =
          {
            (
              queryResults,
              { mutation1, mutation1Results }
            ) => (
              {
                submitToolSuggestion: (name, email, suggestion) =>
                {
                  mutation1(
                    {
                      variables:
                      {
                        name: name,
                        email: email,
                        suggestion: suggestion
                      }
                    }
                  );
                }
              }
            )
          }
          mutation1 = { suggestToolMutation }
        >
          <ToolSuggestionForm />
        </Apollo>
      </section>
    </ToolsLayout>
  );
}

export default ToolsSuggestPage;
