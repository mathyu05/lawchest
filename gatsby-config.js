require("dotenv").config();

module.exports = {
  siteMetadata: {
    title: `LawChest`,
    description: `A War Chest for Australian Lawyers`,
    author: `Matthew Baldwin`,
    tools: [
      {
        id: 1,
        name: "Penalty Units Calculator",
        description: "Penalty units are used in Australia's legal jurisdictions to describe the amount of a fine.  Use this tool to calculate the maximum fine amount given the maximum penalty unit value.",
        link: "/tools/penaltyunits"
      },
      {
        id: 2,
        name: "Pre-Sentence Detention Calculator",
        description: "Description to come...",
        link: "/tools/presentencedetention"
      },
      {
        id: 999,
        name: "Suggest a Tool or Feature for LawChest",
        description: "LawChest strives to be a one-stop shop for the needs of Australia's Legal experts.  Your suggestions for new Tools and Features are always welcome.",
        link: "/tools/suggest"
      }
    ]
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `LawChest`,
        short_name: `LawChest`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#4dc0b5`,
        display: `minimal-ui`,
        icon: `src/images/chest-icon.png`
      }
    },
    `gatsby-plugin-postcss`,
    {
      resolve: `gatsby-plugin-purgecss`,
      options: {
        tailwind: true,
        purgeOnly: [`src/css/style.css`]
      }
    },
    `gatsby-plugin-offline`
  ]
};
