const faunadb = require("faunadb");
require("dotenv").config();

const query = faunadb.query;

function createClient()
{
  if (!process.env.FAUNA_SERVER_SECRET)
  {
    throw new Error(
      "Nope"
    );
  }

  const client = new faunadb.Client(
    {
      secret: process.env.FAUNA_SERVER_SECRET
    }
  );

  return client;
}

exports.client = createClient();
exports.query = query;