const { v4 } = require("uuid");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

exports.resolvers =
{
  Query:
  {
    allJurisdictions: (obj, args, { client, query: q }) =>
    {
      return client
        .query(
          q.Map(
            q.Paginate(q.Match(q.Index("allJurisdictions"))),
            q.Lambda("ref", q.Select(["data"], q.Get(q.Var("ref"))))
          )
        )
        .then(result => result.data)
      ;
    },
    jurisdictionById: (obj, args, { client, query: q }) =>
    {
      return client
        .query(
          q.Get(q.Match(q.Index("jurisdictionById"), args.id))
        )
        .then(result => result.data)
      ;
    },
    penaltyUnitMultipliersByJurisdictionId: (obj, args, { client, query: q }) =>
    {
      return client
        .query(
          q.Map(
            q.Paginate(q.Match(q.Index("penaltyUnitMultipliersByJurisdictionId"), args.jurisdictionId)),
            q.Lambda("ref", q.Select(["data"], q.Get(q.Var("ref"))))
          )
        )
        .then(result => result.data)
      ;
    },
    penaltyUnitMultiplierById: (obj, args, { client, query: q }) =>
    {
      return client
        .query(
          q.Get(q.Match(q.Index("penaltyUnitMultiplierById"), args.id))
        )
        .then(result => result.data)
      ;
    },
    currentUser: (obj, args, { user }) =>
    {
      return user;
    },
    allUsers: (obj, args, { client, query: q, user }) =>
    {
      if (!user)
      {
        throw new Error("You are not logged in to access this information");
      }

      return client
        .query(
          q.Map(
            q.Paginate(q.Match(q.Index("allUsers"))),
            q.Lambda("ref", q.Select(["data"], q.Get(q.Var("ref"))))
          )
        )
        .then(result => result.data)
      ;
    },
    userByEmail: (obj, args, { client, query: q, user }) =>
    {
      if (!user)
      {
        throw new Error("You are not logged in to access this information");
      }

      return client
        .query(
          q.Get(q.Match(q.Index("userByEmail"), args.email))
        )
        .then(result => result.data)
      ;
    }
  },
  Mutation:
  {
    createJurisdiction: (obj, args, { client, query: q, user }) =>
    {
      if (!user)
      {
        throw new Error("You are not logged in to perform this mutation");
      }

      return client
        .query(
          q.Create(
            q.Collection("Jurisdiction"),
            {
              data:
              {
                id: v4(),
                name: args.name
              }
            }
          )
        )
        .then(result => result.data)
      ;
    },
    updateJurisdiction: (obj, args, { client, query: q, user }) =>
    {
      if (!user)
      {
        throw new Error("You are not logged in to perform this mutation");
      }
      
      return client
        .query(
          q.Update(
            q.Select(["ref"], q.Get(q.Match(q.Index("jurisdictionById"), args.id))),
            {
              data:
              {
                name: args.name
              }
            }
          )
        )
        .then(result => result.data)
      ;
    },
    deleteJurisdiction: (obj, args, { client, query: q, user }) =>
    {
      if (!user)
      {
        throw new Error("You are not logged in to perform this mutation");
      }
      
      return client
        .query(
          q.Delete(
            q.Select(["ref"], q.Get(q.Match(q.Index("jurisdictionById"), args.id))
            )
          )
        )
        .then(result => result.data)
      ;
    },
    createPenaltyUnitMultiplier: (obj, args, { client, query: q, user }) =>
    {
      if (!user)
      {
        throw new Error("You are not logged in to perform this mutation");
      }
      
      return client
        .query(
          q.Create(
            q.Collection("PenaltyUnitMultiplier"),
            {
              data:
              {
                id: v4(),
                commencementDate: args.commencementDate,
                value: args.value,
                authorityName: args.authorityName,
                authorityLink: args.authorityLink,
                jurisdictionId: args.jurisdictionId
              }
            }
          )
        )
        .then(result => result.data)
      ;
    },
    updatePenaltyUnitMultiplier: (obj, args, { client, query: q, user }) =>
    {
      if (!user)
      {
        throw new Error("You are not logged in to perform this mutation");
      }
      
      return client
        .query(
          q.Update(
            q.Select(["ref"], q.Get(q.Match(q.Index("penaltyUnitMultiplierById"), args.id))),
            {
              data:
              {
                commencementDate: args.commencementDate,
                value: args.value,
                authorityName: args.authorityName,
                authorityLink: args.authorityLink,
                jurisdictionId: args.jurisdictionId
              }
            }
          )
        )
        .then(result => result.data)
      ;
    },
    deletePenaltyUnitMultiplier: (obj, args, { client, query: q, user }) =>
    {
      if (!user)
      {
        throw new Error("You are not logged in to perform this mutation");
      }
      
      return client
        .query(
          q.Delete(
            q.Select(["ref"], q.Get(q.Match(q.Index("penaltyUnitMultiplierById"), args.id))
            )
          )
        )
        .then(result => result.data)
      ;
    },
    registerUser: async (obj, args, { client, query: q }) =>
    {
      return client
        .query(
          q.Create(
            q.Collection("User"),
            {
              data:
              {
                id: v4(),
                email: args.email.toLowerCase(),
                password: await bcrypt.hash(args.password, 12)
              }
            }
          )
        )
        .then(result => result.data)
      ;
    },
    loginUser: async (obj, args, { client, query: q, SECRET }) =>
    {
      const user = await client
        .query(
          q.Get(q.Match(q.Index("userByEmail"), args.email.toLowerCase()))
        )
        .then(result => result.data)
      ;

      if (!user)
      {
        throw new Error("No User Found");
      }

      const isValid = await bcrypt.compare(args.password, user.password);

      if (!isValid)
      {
        throw new Error("Incorrect password");
      }

      user.token = jwt.sign(
        {
          user:
          {
            "Id": user.id
          },
        },
        SECRET,
        { expiresIn: "1y" }
      );

      return user;
    },
    updateUser: async (obj, args, { client, query: q, user, SECRET }) =>
    {
      if (!user)
      {
        throw new Error("You are not logged in to perform this mutation");
      }

      const userDetails = await client
        .query(
          q.Get(q.Match(q.Index("userByEmail"), args.currentEmail.toLowerCase()))
        )
        .then(result => result.data)
      ;

      if (!userDetails)
      {
        throw new Error("No User Found");
      }

      const isValid = await bcrypt.compare(args.currentPassword, userDetails.password);

      if (!isValid)
      {
        throw new Error("Incorrect password");
      }

      const updatedUserDetails = client
        .query(
          q.Update(
            q.Select(["ref"], q.Get(q.Match(q.Index("userByEmail"), args.currentEmail.toLowerCase()))),
            {
              data:
              {
                email: args.newEmail.toLowerCase(),
                password: await bcrypt.hash(args.newPassword, 12)
              }
            }
          )
        )
        .then(result => result.data)
      ;

      return updatedUserDetails;
    },
    createToolSuggestion: (obj, args, { client, query: q }) =>
    {
      return client
        .query(
          q.Create(
            q.Collection("ToolSuggestion"),
            {
              data:
              {
                id: v4(),
                name: args.name,
                email: args.email,
                suggestion: args.suggestion
              }
            }
          )
        )
        .then(result => result.data)
      ;
    }
  },
  Jurisdiction:
  {
    penaltyUnitMultipliers: (obj, args, { client, query: q }) =>
    {
      return client
        .query(
          q.Map(
            q.Paginate(q.Match(q.Index("penaltyUnitMultipliersByJurisdictionId"), obj.id)),
            q.Lambda("ref", q.Select(["data"], q.Get(q.Var("ref"))))
          )
        )
        .then(result => result.data)
      ;
    }
  }
};