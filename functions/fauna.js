const { ApolloServer } = require("apollo-server-lambda");
const { typeDefs } = require("./faunaSchema.js");
const { resolvers } = require("./faunaResolvers.js");
const { client, query } = require("./faunaDb.js");
const jwt = require("jsonwebtoken");
require("dotenv").config();

const server = new ApolloServer(
  {
    typeDefs,
    resolvers,
    context: async (req) =>
    {
      if (!req.event || !req.event.headers || !req.event.headers["authentication"])
      {
        return { client, query, user: null, SECRET: process.env.JWT_SECRET };
      }

      const token = await req.event.headers["authentication"];
      let user;

      try
      {
        user = await jwt.verify(token, process.env.JWT_SECRET);
      }
      catch (error)
      {
        console.log(`${error.message} caught`);
      }

      return { client, query, user, SECRET: process.env.JWT_SECRET };
    },
    playground: null
  }
);

exports.handler = server.createHandler();