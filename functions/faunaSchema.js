const { gql } = require("apollo-server-lambda");

exports.typeDefs = gql`
  type Query
  {
    allJurisdictions: [Jurisdiction]
    jurisdictionById(id: ID!): Jurisdiction
    penaltyUnitMultipliersByJurisdictionId(jurisdictionId: ID!): [PenaltyUnitMultiplier]!
    penaltyUnitMultiplierById(id: ID!): PenaltyUnitMultiplier
    currentUser: User
    allUsers: [User]
    userByEmail(email: String!): User
  }

  type Mutation
  {
    createJurisdiction(name: String!): Jurisdiction
    updateJurisdiction(id: ID!, name: String!): Jurisdiction
    deleteJurisdiction(id: ID!): Jurisdiction
    createPenaltyUnitMultiplier(jurisdictionId: ID!, commencementDate: String!, value: Float!, authorityName: String, authorityLink: String): PenaltyUnitMultiplier
    updatePenaltyUnitMultiplier(id: ID!, commencementDate: String!, value: Float!, authorityName: String, authorityLink: String): PenaltyUnitMultiplier
    deletePenaltyUnitMultiplier(id: ID!): PenaltyUnitMultiplier
    registerUser(email: String!, password: String!): User!
    loginUser(email: String!, password: String!): User!
    updateUser(currentEmail: String!, currentPassword: String!, newEmail: String!, newPassword: String!): User!
    createToolSuggestion(name: String, email: String, suggestion: String!): ToolSuggestion
  }

  type Jurisdiction
  {
    id: ID!
    name: String!
    penaltyUnitMultipliers: [PenaltyUnitMultiplier]!
  }

  type PenaltyUnitMultiplier
  {
    id: ID!
    jurisdictionId: ID!
    commencementDate: String!
    value: Float!
    authorityName: String
    authorityLink: String
  }

  type User
  {
    id: ID!
    email: String!
    password: String!
    token: String
  }

  type ToolSuggestion
  {
    id: ID!
    name: String
    email: String
    suggestion: String!
  }
`;