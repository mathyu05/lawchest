const fetch = require("node-fetch");
const { client, query } = require("./functions/faunaDb");
const q = query;

client
  .query(
    q.Map(
      [
        {
          value: 1,
          label: "Victoria",
          penaltyUnitMultipliers: []
        }
      ],
      q.Lambda(
        "jurisdiction",
        q.Create(
          q.Collection("Jurisdiction"),
          { data: q.Var("jurisdiction") }
        )
      )
    )
  )
  .then(console.log("Wrote Jurisdictions to FaunaDB"))
  .catch(error => console.log(error))
;